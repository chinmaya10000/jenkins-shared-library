#!/usr/bin/env groovy

def call(String imageName) {
    echo "build and push the docker image..."
    sh "docker build -t $imageName ."
}